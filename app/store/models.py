from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
from django.contrib.auth import get_user_model

User = get_user_model()


class Category(MPTTModel):
    name = models.CharField(max_length=200, unique=True)
    category_params = models.ManyToManyField('CategoryParams', blank=True)
    parent = TreeForeignKey('self', on_delete=models.CASCADE, blank=True, null=True, related_name='children')

    class Meta:
        verbose_name_plural = 'Category'

    def __str__(self):
        return self.name

    class MPTTMeta:
        order_insertion_by = ['name']


class CategoryParams(models.Model):
    name = models.CharField(max_length=200, unique=True)

    class Meta:
        verbose_name_plural = 'Category params'

    def __str__(self):
        return self.name


class CategoryParamsValue(models.Model):
    category_params = models.ForeignKey(CategoryParams, related_name='cat_params_value', on_delete=models.CASCADE)
    value = models.CharField(max_length=200, unique=True)

    class Meta:
        verbose_name_plural = 'Category params values'

    def __str__(self):
        return self.value


class Product(models.Model):
    category = models.ForeignKey(Category, related_name='product', on_delete=models.CASCADE)
    category_param_value = models.ManyToManyField(CategoryParamsValue, blank=True)
    title = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    is_active = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    price = models.FloatField()
    quantity = models.PositiveIntegerField()
    user_comparison = models.ManyToManyField(User)

    class Meta:
        verbose_name_plural = 'Products'
        ordering = ('-created',)

    def __str__(self):
        return self.title


class ImageProduct(models.Model):
    product = models.ForeignKey(Product, related_name='images', on_delete=models.CASCADE)
    image = models.ImageField('Product image', upload_to='product/%Y/%m/%d/', blank=True)

    class Meta:
        verbose_name_plural = "Product images"
