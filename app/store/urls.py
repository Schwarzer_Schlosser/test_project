from django.urls import path, include
from rest_framework import routers
from .views import CategoryViewSet, ProductViewSet, CategoryParamsViewSet, ComparedViewSet, TestCategoryViewSet


app_name = 'store'

router = routers.SimpleRouter()
router.register(r'parent_category', CategoryViewSet, basename='parent_category')
router.register(r'product', ProductViewSet, basename='product')
router.register(r'category_params', CategoryParamsViewSet, basename='category_params')
router.register(r'compared', ComparedViewSet, basename='compared')

router.register(r'test', TestCategoryViewSet, basename='test')


urlpatterns = [
    path('', include(router.urls)),
]
