from django.contrib import admin
from django_admin_listfilter_dropdown.filters import RelatedDropdownFilter, DropdownFilter

from .models import Category, Product, CategoryParams, CategoryParamsValue, ImageProduct
from rangefilter.filters import NumericRangeFilter


class CategoryInline(admin.TabularInline):
    model = Category.category_params.through
    verbose_name_plural = 'Category params'


# ########
# class CategoryParamsValueInline(admin.TabularInline):
#     model = CategoryParamsValue
#
#
# class ProductInline(admin.TabularInline):
#     model = Product
#     filter_vertical = ['category_param_value']
# #######

@admin.register(Category)
class AdminCategory(admin.ModelAdmin):
    list_display = ('name', 'children_display')
    # list_display = ('name',)
    # inlines = [CategoryInline, ProductInline]
    inlines = [CategoryInline]
    exclude = ['category_params']

    def children_display(self, obj):
        return ", ".join([child.name for child in obj.children.all()])


@admin.register(CategoryParams)
class AdminCategoryParams(admin.ModelAdmin):
    list_display = ('name',)
    # inlines = [CategoryParamsValueInline]


@admin.register(CategoryParamsValue)
class AdminCategoryParamsValue(admin.ModelAdmin):
    list_display = ('value',)


class CategoryParamsValueInline(admin.TabularInline):
    model = Product.category_param_value.through

    verbose_name_plural = 'Category params values'


class ImageProductInline(admin.TabularInline):
    model = ImageProduct


@admin.register(Product)
class AdminProduct(admin.ModelAdmin):
    list_display = ('category', 'title', 'price', 'is_active')
    fields = ('title', 'description', ('category', 'price'), 'is_active', 'quantity')
    search_fields = ('title',)
    list_filter = ('is_active', ('category', RelatedDropdownFilter), ('price', NumericRangeFilter))
    inlines = [CategoryParamsValueInline, ImageProductInline]
    exclude = ['category_param_value']
