from django_filters import rest_framework as filters
from .models import Product, Category


class ProductFilter(filters.FilterSet):
    price_max = filters.NumberFilter(field_name='price', lookup_expr='gte')
    price_min = filters.NumberFilter(field_name='price', lookup_expr='lte')
    category = filters.NumberFilter(field_name='category__id', method='filter_category')

    class Meta:
        model = Product
        fields = ('price_min', 'price_max', 'category')

    def filter_category(self, queryset, name, value):
        try:
            category = Category.objects.get(id=value)
        except:
            return queryset
        category_queryset_children = category.get_descendants(include_self=True)
        return queryset.filter(category__in=category_queryset_children)
