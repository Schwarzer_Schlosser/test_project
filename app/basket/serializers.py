from django.contrib.auth import get_user_model
from rest_framework import serializers
from django.db.models import F

from .models import CartItem

User = get_user_model()


class CreateCartItemSerializer(serializers.ModelSerializer):

    class Meta:
        model = CartItem
        fields = ('product',)

    def create(self, validated_data):
        user = self.context['request'].user
        product = validated_data.get('product')
        validated_data.setdefault('user', user)
        if user.cart_items.filter(product__id=product.id).exists():
            cart_item = user.cart_items.filter(product__id=product.id)
            cart_item.update(quantity=F("quantity") + 1)
            cart_item_obj = cart_item.first()
        else:
            cart_item_obj = CartItem.objects.create(**validated_data)
        return cart_item_obj


class UpdateCartItemSerializer(serializers.ModelSerializer):

    class Meta:
        model = CartItem
        fields = ('id', 'quantity')

    def validate_quantity(self, value):
        product_quantity = self.instance.product.quantity
        if int(value) > product_quantity:
            raise serializers.ValidationError(f'There is no such quantity {value}, in stock {product_quantity}.')
        return value

    def update(self, instance, validated_data):
        new_quantity = validated_data.get('quantity')
        instance.quantity = new_quantity
        instance.save()
        return instance


class ListCartItemSerializer(serializers.ModelSerializer):
    total_price = serializers.IntegerField(source='tot_price')

    class Meta:
        model = CartItem
        fields = ('id', 'user', 'quantity', 'product', 'total_price')


class CartItemSerializer(serializers.ModelSerializer):
    total_price = serializers.SerializerMethodField()

    class Meta:
        model = CartItem
        fields = ('id', 'user', 'quantity', 'product', 'total_price')

    def get_total_price(self, obj):
        return obj.quantity * obj.product.price


class CartSerializer(serializers.ModelSerializer):
    cart_item = CartItemSerializer(source='cart_items', many=True)
    all_price = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'email', 'cart_item', 'all_price')

    def get_all_price(self, obj):
        cart_items = obj.cart_items.all()
        return sum([items.quantity * items.product.price for items in cart_items])
