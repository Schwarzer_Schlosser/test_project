from django.db import models
from django.contrib.auth import get_user_model
from store.models import Product

User = get_user_model()


class CartItem(models.Model):
    user = models.ForeignKey(User, related_name='cart_items', on_delete=models.CASCADE)
    product = models.ForeignKey(Product, related_name='cart_items', on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField(default=1)

    class Meta:
        verbose_name_plural = 'Cart items'

    def __str__(self):
        return f'{self.pk} -- {self.product}'
