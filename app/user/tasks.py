from django.contrib.auth import get_user_model
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.urls import reverse
from rest_framework_simplejwt.tokens import RefreshToken

from app.celery import app

User = get_user_model()


@app.task()
def send_email_for_verification(email, site_domain):
    user = User.objects.get(email=email)

    context = {
        'user': user,
        'domain': site_domain,
        'token': RefreshToken.for_user(user).access_token,
        'uri': reverse('user:email_verify')
    }
    message = render_to_string(
        'registration/email_verification.html',
        context=context,
    )
    email = EmailMessage(
        'Verify email',
        message,
        to=[user.email]
    )
    email.send()


@app.task()
def send_email_for_change_password(encoded_pk, token, site_domain, email):

    context = {
        'user': email,
        'domain': site_domain,
        'uri': reverse('user:reset_password', kwargs={'encoded_pk': encoded_pk, 'token': token})
    }
    message = render_to_string(
        'registration/password_reset_confirmation.html',
        context=context,
    )
    email = EmailMessage(
        'Verify email',
        message,
        to=[email]
    )
    email.send()
