from django.contrib.auth import authenticate, get_user_model
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils.http import urlsafe_base64_decode
from rest_framework import serializers
from rest_framework.exceptions import AuthenticationFailed
from rest_framework_simplejwt.tokens import RefreshToken, TokenError
from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from .models import City, Country, State

User = get_user_model()


class RegistrationSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=150, write_only=True)
    password_conf = serializers.CharField(max_length=150, write_only=True)

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email', 'password', 'password_conf')

    def validate(self, attrs):
        errors = {}
        password = attrs.get('password', None)
        password_conf = attrs.get('password_conf', None)

        if password and password_conf and password != password_conf:
            errors['password'] = ['Password mismatch']
        try:
            validate_password(password)
        except ValidationError as e:
            [errors['password'].append(message)
             for message in e.messages] if errors else errors.setdefault('password', e.messages)

        if errors:
            raise serializers.ValidationError(errors)

        return super().validate(attrs)

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)


class EmailVerificationSerializer(serializers.ModelSerializer):
    token = serializers.CharField(max_length=555)

    class Meta:
        model = User
        fields = ('token',)


class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField(max_length=255, min_length=3)
    password = serializers.CharField(max_length=68, min_length=6, write_only=True)
    tokens = serializers.SerializerMethodField()

    class Meta:
        fields = ('email', 'password', 'tokens',)

    def get_tokens(self, obj):
        user = User.objects.get(email=obj.email)

        return {
            'refresh': user.tokens()['refresh'],
            'access': user.tokens()['access']
        }

    def validate(self, attrs):
        email = attrs.get('email', None)
        password = attrs.get('password', None)

        user = authenticate(email=email, password=password)

        if not user:
            raise serializers.ValidationError('A user with this email and password was not found.')
        if not user.is_verified:
            raise serializers.ValidationError('Email is not verified')

        return user


class LogoutSerializer(serializers.Serializer):
    refresh = serializers.CharField()

    def validate(self, attrs):
        self.token = attrs['refresh']
        return attrs

    def save(self, **kwargs):
        try:
            RefreshToken(self.token).blacklist()
        except TokenError as ex:
            raise AuthenticationFailed(ex)


class UpdateUserInfoSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'email', 'first_name', 'last_name', 'date_of_birth', 'country', 'state', 'city',)
        read_only_fields = ('id', 'email',)


class ResetUserPasswordSerializer(serializers.Serializer):
    email = serializers.EmailField(min_length=2)

    class Meta:
        fields = ('email',)


class ConfirmResetUserPasswordSerializer(serializers.Serializer):
    password = serializers.CharField(write_only=True)

    class Meta:
        field = ('password',)

    def validate(self, attrs):

        password = attrs.get('password', None)
        token = self.context.get('kwargs').get('token', None)
        encoded_pk = self.context.get('kwargs').get('encoded_pk', None)

        if token is None or encoded_pk is None:
            raise serializers.ValidationError('Missing data.')

        try:
            validate_password(password)
        except ValidationError as e:
            raise serializers.ValidationError(e.messages)

        pk = urlsafe_base64_decode(encoded_pk).decode()
        user = User.objects.get(pk=pk)

        if not PasswordResetTokenGenerator().check_token(user, token):
            raise serializers.ValidationError('The reset token is invalid')

        user.set_password(password)
        user.save()
        return super().validate(attrs)


class ChangePassSerializer(serializers.Serializer):
    old_password = serializers.CharField(max_length=128, write_only=True, required=True)
    password = serializers.CharField(max_length=128, write_only=True, required=True)
    password_conf = serializers.CharField(max_length=128, write_only=True, required=True)

    class Meta:
        fields = ('old_password', 'password', 'password_conf')

    def validate(self, attrs):

        errors = {}
        password = attrs.get('password', None)
        password_conf = attrs.get('password_conf', None)
        old_password = attrs.get('old_password', None)
        if password and password_conf and password != password_conf:
            errors['password'] = ['Password mismatch']
        try:
            validate_password(password)
        except ValidationError as e:
            [errors['password'].append(message)
             for message in e.messages] if errors else errors.setdefault('password', e.messages)

        if errors:
            raise serializers.ValidationError(errors)

        if not self.instance.check_password(old_password):
            raise serializers.ValidationError({'old_password': 'Wrong password.'})

        return super().validate(attrs)


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ('id', 'name')


class StateSerializer(serializers.ModelSerializer):
    cities = serializers.StringRelatedField(source='city_set', many=True)

    class Meta:
        model = State
        fields = ('id', 'name', 'country', 'cities')


class CountrySerializer(serializers.ModelSerializer):
    states = serializers.StringRelatedField(source='state_set', many=True)

    class Meta:
        model = Country
        fields = ('id', 'name', 'sortname', 'states',)


class ChangeUserEmailSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('email',)


# TODO implement
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer