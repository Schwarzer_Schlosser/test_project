import json

from celery.utils.serialization import jsonify
from django.http import HttpResponse, JsonResponse
from django.shortcuts import redirect, render
import stripe
from django.conf import settings
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt

from order.models import Order, OrderItem
from basket.models import CartItem
from store.models import Product


stripe.api_key = settings.STRIPE_API_KEY
endpoint_secret = settings.STRIPE_SECRET_WEBHOOK
SITE_DOMAIN = settings.SITE_DOMAIN


def calculate_order_amount(items):
    # Replace this constant with a calculation of the order's amount
    # Calculate the order total on the server to prevent
    # people from directly manipulating the amount on the client
    return 1400
#
#
# class CreateCheckoutSessionView(View):
#
#     def post(self, request, *args, **kwargs):
#     #     print(request.body)
#     #     print(request.get_host)
#     #     print(request.get_port)
#     #     print(request.headers)
#     #     print(request.method)
#     #     print(request.path)
#     #     print(request.user)
#     #     print(dir(request))
#     #
#     #     checkout_session = stripe.checkout.Session.create(
#     #
#     #         line_items=[
#     #             {
#     #                 'price': 'price_1MlZ5gDsjXvsfeOtsEh0pd5O',
#     #                 'quantity': 1,
#     #             },
#     #         ],
#     #         mode='payment',
#     #
#     #         success_url=f'http://{SITE_DOMAIN}/payments/home/',
#     #         cancel_url=f'http://{SITE_DOMAIN}',
#     #     )
#     #     # print(checkout_session)
#     #     return redirect(checkout_session.url, code=303)
#
#         try:
#             data = json.loads(request.data)
#             # Create a PaymentIntent with the order amount and currency
#             intent = stripe.PaymentIntent.create(
#                 amount=calculate_order_amount(data['items']),
#                 currency='usd',
#                 automatic_payment_methods={
#                     'enabled': True,
#                 },
#             )
#             return jsonify({
#                 'clientSecret': intent['client_secret']
#             })
#         except Exception as e:
#             return jsonify(error=str(e)), 403


def home(request):
    return render(request, 'home.html')


@csrf_exempt
def stripe_webhook(request):
    payload = request.body
    print(request)
    sig_header = request.META['HTTP_STRIPE_SIGNATURE']
    event = None

    try:
        event = stripe.Webhook.construct_event(
            payload, sig_header, settings.STRIPE_SECRET_WEBHOOK
        )
    except ValueError as e:

        return HttpResponse(status=400)
    except stripe.error.SignatureVerificationError as e:

        return HttpResponse(status=400)

    # Handle the checkout.session.completed event
    if event['type'] == 'checkout.session.completed':
        # Retrieve the session. If you require line items in the response, you may include them by expanding line_items.
        session = event['data']['object']

        print(session)

    # Passed signature verification
    return HttpResponse(status=200)


@method_decorator(csrf_exempt, name='dispatch')
class StripeIntentView(View):

    def post(self, request, *args, **kwargs):
        try:

            print(self.kwargs)
            print(self.args)
            print(self.request)
            print(request.body)
            print(request)

            intent = stripe.PaymentIntent.create(
                amount=12*100,
                currency='usd'
                # metadata={
                #     "product_id": product.id
                # }
            )
            # print(intent)
            return JsonResponse({
                'clientSecret': intent['client_secret']
            })
        except Exception as e:
            return JsonResponse({'error': str(e)})
        # try:
        #     data = request
        #     # Create a PaymentIntent with the order amount and currency
        #     intent = stripe.PaymentIntent.create(
        #         amount=calculate_order_amount(data['items']),
        #         currency='aud',
        #         automatic_payment_methods={
        #             'enabled': True,
        #         },
        #     )
        #     return jsonify({
        #         'clientSecret': intent['client_secret']
        #     })
        # except Exception as e:
        #     return JsonResponse({'error': str(e)})
