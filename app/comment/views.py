from rest_framework.mixins import CreateModelMixin, DestroyModelMixin, ListModelMixin, RetrieveModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import GenericViewSet

from .models import Review, ReviewComment
from .serializers import ReviewCommentSerializer, ReviewSerializer


class ReviewViewSet(CreateModelMixin, RetrieveModelMixin, ListModelMixin, DestroyModelMixin, GenericViewSet):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer
    permission_classes = [IsAuthenticated]


class ReviewCommentViewSet(CreateModelMixin, GenericViewSet):
    queryset = ReviewComment.objects.all()
    serializer_class = ReviewCommentSerializer
