from django.contrib import admin

from .models import Review, ReviewComment


class ReviewCommentInline(admin.TabularInline):
    model = ReviewComment
    # readonly_fields = ['username', 'review_text', 'created']


@admin.register(Review)
class AdminReview(admin.ModelAdmin):
    inlines = [ReviewCommentInline]
    readonly_fields = ['user', 'review_text', 'created', 'rating', 'product']
