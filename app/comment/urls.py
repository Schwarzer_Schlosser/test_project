from django.urls import include, path
from rest_framework import routers

from .views import ReviewCommentViewSet, ReviewViewSet

app_name = 'comment'

router = routers.SimpleRouter()
router.register(r'review', ReviewViewSet, basename='review')
router.register(r'review-comment', ReviewCommentViewSet, basename='review_comment')

urlpatterns = [
    path('', include(router.urls)),
]
