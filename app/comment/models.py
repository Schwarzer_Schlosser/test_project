from django.contrib.auth import get_user_model
from django.db import models

from store.models import Product
from .utils import RatingChoices

User = get_user_model()


class Base(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, related_name='%(class)s', blank=True, null=True)
    review_text = models.TextField()
    created = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Review(Base):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='reviews')
    rating = models.PositiveSmallIntegerField(choices=RatingChoices.choices, blank=True, null=True)

    class Meta:
        verbose_name_plural = 'Reviews'

    def __str__(self):
        return f'{self.user.first_name} {self.user.last_name}'


class ReviewComment(Base):
    review = models.ForeignKey(Review, on_delete=models.CASCADE, related_name='review_comments')
    username = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        verbose_name_plural = 'Review comments'

    def __str__(self):
        return self.username
