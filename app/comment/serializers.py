from rest_framework import serializers

from order.models import OrderItem
from .models import Review, ReviewComment


class ReviewCommentSerializer(serializers.ModelSerializer):
    username = serializers.CharField(max_length=50, required=True)

    class Meta:
        model = ReviewComment
        fields = ('review', 'id', 'username', 'review_text', 'created')
        read_only_fields = ('id', 'created')

    def create(self, validated_data):
        user = self.context['request'].user
        if user.is_authenticated:
            validated_data.update(user=user)
        comment_obj = ReviewComment.objects.create(**validated_data)
        return comment_obj


class ReviewSerializer(serializers.ModelSerializer):
    review_id = serializers.IntegerField(source='id', read_only=True)
    review_comments = ReviewCommentSerializer(many=True, read_only=True)

    class Meta:
        model = Review
        fields = ('review_id', 'user', 'product', 'rating', 'review_text', 'created', 'review_comments')
        read_only_fields = ('user', 'review_id', 'created')

    def validate_product(self, value):
        user = self.context['request'].user
        purchased_product = OrderItem.objects.filter(order__buyer=user).values_list('product', flat=True)
        if value.id not in purchased_product:
            raise serializers.ValidationError('The user did not buy this product.')
        return value

    def create(self, validated_data):
        user = self.context['request'].user
        if validated_data.get('rating', None):
            Review.objects.filter(user=user, rating__in=range(1, 6)).update(rating=None)
        validated_data.update(user=user)
        review_obj = Review.objects.create(**validated_data)
        return review_obj
