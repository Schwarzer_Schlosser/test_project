from django.contrib import admin

from .models import Order, OrderItem


class OrderItemInline(admin.TabularInline):
    model = OrderItem
    readonly_fields = ['order']


@admin.register(Order)
class AdminOrder(admin.ModelAdmin):
    readonly_fields = ['buyer', 'total_price', 'delivery', 'email', 'created_at', 'updated_at']
    inlines = [OrderItemInline]
