from rest_framework import serializers

from .models import Order, OrderItem


class OrderItemSerializer(serializers.ModelSerializer):
    cost_price = serializers.SerializerMethodField()

    class Meta:
        model = OrderItem
        fields = ('id', 'product', 'price', 'quantity', 'cost_price')

    def get_cost_price(self, obj):
        return obj.get_cost()


class OrderSerializer(serializers.ModelSerializer):
    ord_items = OrderItemSerializer(source='items', many=True, read_only=True)

    class Meta:
        model = Order
        fields = (
            'id', 'buyer', 'status', 'email', 'phone_number', 'shipping_first_name', 'shipping_last_name',
            'shipping_surname', 'shipping_city', 'total_price', 'paid', 'delivery', 'ord_items'
        )
        read_only_fields = ('buyer', 'paid', 'status', 'total_price')


class NovaPostaOfficeSerializer(serializers.Serializer):
    site_key = serializers.IntegerField(source='SiteKey')
    description = serializers.CharField(source='Description', max_length=300)
    description_ru = serializers.CharField(source='DescriptionRu', max_length=300)
    phone = serializers.CharField(source='Phone', max_length=20)
    schedule = serializers.DictField(source='Schedule')
    status = serializers.CharField(source='WarehouseStatus', max_length=50)
    max_height = serializers.IntegerField(source='ReceivingLimitationsOnDimensions.Height')
    max_weight = serializers.IntegerField(source='PlaceMaxWeightAllowed')


class NovaPostaLocalitySerializer(serializers.Serializer):
    description = serializers.CharField(source='Description', max_length=200)
    description_ru = serializers.CharField(source='DescriptionRu', max_length=200)
    type_description = serializers.CharField(source='SettlementTypeDescription', max_length=100)
    type_description_ru = serializers.CharField(source='SettlementTypeDescriptionRu', max_length=100)
    area_description = serializers.CharField(source='AreaDescription', max_length=100)
    area_description_ru = serializers.CharField(source='AreaDescriptionRu', max_length=100)


class LocalitySerializer(serializers.Serializer):
    locality = serializers.CharField(min_length=3, max_length=50, required=True)

    class Meta:
        fields = ('locality',)
