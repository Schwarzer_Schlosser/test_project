from django.urls import path, include
from rest_framework import routers
from .views import OrderViewSet, NovaPostaOfficeView, NovaPostaLocalityView


app_name = 'order'

router = routers.SimpleRouter()
router.register(r'', OrderViewSet, basename='order')

urlpatterns = [
    path('nova-posta-office/', NovaPostaOfficeView.as_view(), name='nova_posta_office'),
    path('nova-posta-locality/', NovaPostaLocalityView.as_view(), name='nova_posta_locality'),
    path('', include(router.urls)),
]
