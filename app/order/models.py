from django.db import models
from .utils import OrderStatusChoices
from django.contrib.auth import get_user_model
from store.models import Product

User = get_user_model()


class Order(models.Model):
    buyer = models.ForeignKey(User, related_name='orders', on_delete=models.CASCADE)
    status = models.CharField(choices=OrderStatusChoices.choices, max_length=20, default=OrderStatusChoices.NEW)
    total_price = models.FloatField(blank=True)
    delivery = models.CharField(max_length=350)
    email = models.EmailField()
    phone_number = models.CharField(max_length=50)
    shipping_first_name = models.CharField(max_length=100)
    shipping_last_name = models.CharField(max_length=100)
    shipping_surname = models.CharField(max_length=100)
    shipping_city = models.CharField(max_length=50)
    paid = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = 'Orders'

    def __str__(self):
        return f'Order {self.id} - {self.buyer}'


class OrderItem(models.Model):
    order = models.ForeignKey(Order, related_name='items',  on_delete=models.CASCADE)
    product = models.ForeignKey(Product, related_name='order_items', on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    quantity = models.PositiveIntegerField(default=1)

    def __str__(self):
        return f'{self.id}'

    def get_cost(self):
        return self.price * self.quantity
