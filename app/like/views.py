from django.db.models import Sum
from drf_spectacular.types import OpenApiTypes
from drf_spectacular.utils import extend_schema, OpenApiParameter
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.mixins import CreateModelMixin, DestroyModelMixin, ListModelMixin, UpdateModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from .models import LikeList
from .serializers import AddLikeListSerializer, CreateLikeListSerializer, LikeListSerializer, UpdateLikeListSerializer


class LikeListViewSet(ListModelMixin, CreateModelMixin, DestroyModelMixin, UpdateModelMixin, GenericViewSet):
    queryset = LikeList.objects.all()
    permission_classes = [IsAuthenticated]
    http_method_names = ['post', 'patch', 'delete', 'get']

    def get_queryset(self):
        queryset = super().get_queryset()
        if self.action == 'list':
            queryset = queryset.filter(user=self.request.user).annotate(tot_price=Sum('product__price'))
        return queryset

    def get_serializer_class(self):
        if self.action == 'create':
            return CreateLikeListSerializer
        if self.action == 'list':
            return LikeListSerializer
        if self.action == 'partial_update':
            return UpdateLikeListSerializer
        if self.action in 'add_product_in_like':
            return AddLikeListSerializer

    @action(methods=['post'], detail=False, serializer_class=AddLikeListSerializer, permission_classes=[IsAuthenticated])
    def add_product_in_like(self, request, *args, **kwargs):
        product, = self.request.data.get('product')
        user = self.request.user
        serializer = self.serializer_class(data=self.request.data)
        serializer.is_valid(raise_exception=True)

        if not LikeList.objects.filter(user=user, is_default=True).exists():
            LikeList.objects.create(title='My wish list', user=user, is_default=True).product.add(product)
        def_like_list = LikeList.objects.get(user=user, is_default=True)
        def_like_list.product.add(product)

        return Response(serializer.data)

    params = OpenApiParameter(
        name='params',
        type=OpenApiTypes.INT,
        location=OpenApiParameter.QUERY,
        description='Id product'
    )

    @extend_schema(parameters=[params, ])
    @action(methods=['delete'], detail=False, permission_classes=[IsAuthenticated])
    def remove_product_from_like(self, request, *args, **kwargs):
        user = self.request.user
        product = self.request.query_params.get('params')
        if LikeList.objects.filter(user=user, is_default=True, product=product).exists():
            def_like_list = LikeList.objects.get(user=user, product=product)
            def_like_list.product.remove(product)
            return Response(status=status.HTTP_204_NO_CONTENT)
        return Response({"Message": "Product doesn't exists in Wish List"}, status=status.HTTP_400_BAD_REQUEST)
