from django.urls import path, include
from rest_framework import routers
from .views import LikeListViewSet


app_name = 'like'

router = routers.SimpleRouter()
router.register(r'like_list', LikeListViewSet, basename='like_list')

urlpatterns = [
    path('', include(router.urls)),
]
